---
layout: job_family_page
title: "Internal Audit Manager"
---

GitLab is adding the next essential member who can add in-depth internal audit to the finance team. You will join our team in its early stages responsible for developing a highly efficient, world class audit process

## Responsibilities

* Primary responsibility for determining,documenting and implementing the Company’s internal audit policies.							
* Document financial processes and perform test of controls for SOX compliance and audits.
* Assist with the execution of the internal audit plan and timely completion of assigned audits.
* Assist with enterprise risk management activities.
* Recommend improvements related to the Company’s key controls.
* Work closely with external auditors.
* Must be able to work collaboratively with the operational accounting team and business functions.
* Responds to inquiries from the CFO, Controller, and company wide managers regarding financial results, special reporting requests and the like.
* Act as a subject matter expert, working with the business partners in accounting and other functions (e.g., legal, corporate development, stock administration) to identify financial risks associated with new or contemplated transactions and resolve complex accounting issues.
* Participate in team planning including setting team goals and priorities, monitoring progress and removing roadblocks.
* This position will initially be an individual contributor but we expect this person to build a highly functioning, distributed team over time.
 
## Requirements

* Proven work experience as a Technical Accounting or similar leadership role.
* Must have at least 5 years experience in a big 4 public accounting firm.
* Certified Public Accountant desirable.
* Strong technical, analytic, and communication skills (both written and verbal).
* In-depth knowledge of SEC filing requirements, experience highly preferred.
* Must have direct knowledge of US GAAP.
* Must have public company experience with Sarbanes Oxley.
* Ability to contribute to the career development of staff and a culture of teamwork.
* Strong working knowledge of GAAP principles and financial statements.
* Proficient with google sheets and GSuite.
* Revenue recognition, namely 606, experience highly preferred. 
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision.
* Ability to balance quality of work with speed of execution.




## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
