---
layout: job_family_page
title: "Sourcer"
---

Sourcers at GitLab are experts at finding the best candidates for a high-tech, distributed company. 
They work closely with the broader Recruiting team on building sourcing strategies, market mapping, 
and finding the best talent in the world.

This role will report to the Sourcing Manager.

## Sourcer (Intermediate)

## Responsibilities

* Source for the best passive candidates in the global market.
* Being willing to look wherever is necessary to find the talent we need, with a focus on our values and requirements.
* Put meaningful focus on building a diverse pipeline.
* Partner directly with in-house recruiters and business leaders globally to understand specific needs to attract and engage with top talent.
* Continuously work on streamlining sourcing processes and delivering great candidate experience.
* Strategically utilize LinkedIn, online research, events, etc. to engage highly passive, sought after candidates.
* Develop a strong relationship with candidates and make judicious decisions on fit for a particular role or team, in addition to thinking through fit for our unique culture.
* Map out individual markets and gather intelligence around specific talent pools, using that knowledge to identify top talent.

## Requirements

* Experience sourcing and research at all levels, preferably in a global capacity within the software industry, open source experience is a plus.
* Proven success in sourcing for technical and/or sales.
* Demonstrated ability to effectively source passive candidates. This is a fully outbound role.
* Experience with competitive global job markets preferred.
* Previous experience in sourcing in low-cost regions for engineering/development talent would be a plus.
* Focused on delivering an excellent candidate experience.
* Ambitious, efficient, and stable under tight deadlines and competing priorities.
* Remote working experience in a technology startup will be an added advantage.
* Ability to build relationships with managers and colleagues across multiple disciplines and timezones.
* Working knowledge using an candidate tracking systems. Greenhouse is a plus.
* Outstanding written and verbal communication skills across all levels.
* Willingness to learn and use software tools including Git and GitLab.
* College / University degree in Marketing, Human Resources, or related field from an accredited institution preferred.
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).

## Senior Sourcer

Senior sourcers share the same requirements as the Intermediate sourcer listed above, but also carry the following:

- Assist with evaluation and implementation of new sourcing tools and procedures
- Researching and understanding the market to define target searches
- Leverage data to improve efficiency, drive innovation and influence hiring decisions
- Serve as a mentor for the sourcing team through Leading by example, strong performance, and problem solving
- Serve as the primary sourcer for senior and executive level roles

## Sourcing Manager

## Responsibilities

* Serve as the primary sourcer (~50% of capacity) for senior and executive level roles
* Lead and coach a team of a remote based sourcing team that can scale to the dynamic demands of a rapidly growing world-wide technology company
* Onboard, mentor, and grow the careers of all team members
* Partner with stakeholders to develop strategies, priorities and process improvements according to fluctuating business demand
* Partner with recruiting and other internal functional group leaders to ensure the overall team understands business needs and meets hiring metrics
* Turn business requirements into effective sourcing strategies and deliver on the goals
* Manage and improve consistent data-driven sourcing metrics
* Lead trainings and disseminate best practices to team as a resident sourcing expert.
* Innovate and operationalize sourcing methods to deliver diverse talent to GitLab
* Provide coaching to improve performance of team members and drive accountability
* Deep understanding of the business and ability to align sourcing support based on priority and impact.
* Consistent and effective communication to all stakeholders to ensure alignment and clear understanding of priorities

## Requirements

* Minimum of 2-3 years of people management experience
* 3 years of sourcing or recruiting experience
* Expertise and a proven track record of developing recruiting best practices, and candidate sourcing strategies.
* Proficient in analyzing data, presenting solutions, and making data-driven decisions
* Experience effectively sourcing for diverse candidates
* Passionate about building teams, growing careers, and inspiring people
* Experience sourcing and research at all levels, preferably in a global capacity within the software industry, open source experience is a plus.
* Experience with competitive global job markets preferred.
* Focused on delivering an excellent candidate experience.
* Focused on efficiency, scalability and constant process improvements

## Performance indicators

* [Prospects Submitted](https://about.gitlab.com/handbook/hiring/metrics/#sourced-prospects-submitted)
* [Phone Screens Scheduled](https://about.gitlab.com/handbook/hiring/metrics/#phone-screens-scheduled-by-a-sourcer)
* [Interview Conversion Rate](https://about.gitlab.com/handbook/hiring/metrics/#sourcer-interview-convertion-rate)
* [Offer Conversion Rate](https://about.gitlab.com/handbook/hiring/metrics/#sourcer-offer-convertion-rate)
* [Hires Conversion Rate](https://about.gitlab.com/handbook/hiring/metrics/#sourcer-hires-conversion-rate)
* [Number of candidates moved to the Reference Check](https://about.gitlab.com/handbook/hiring/metrics/#sourced-candidates-moved-to-the-reference-check)
* [Number of Offers sourced](https://about.gitlab.com/handbook/hiring/metrics/#sourced-offers)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with our Sourcing Manager.
* Next, candidates will be invited to schedule a 45 minute interview with one of our Sourcers.
* Next, candidates will be invited to schedule a 30 minute interview with one of our Recruiters.
* Next, candidates may be invited to a 45 minute interview with our Sourcing Manager.
* Then, candidates will be invited to schedule a 30 minute interview with one of our Recruiting Managers.
* Finally, our CEO may choose to conduct a final interview.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
