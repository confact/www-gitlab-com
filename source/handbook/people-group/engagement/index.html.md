---
layout: markdown_page
title: "Engagement"
---
### Engagement

Team member engagement is important to the overall continued success of GitLab.  Engagement strategies have been shown to reduce attrition, improve productivity and efficiency, enhances the company values and enhanced job satisfaction.  One of the ways that GitLab tracks team member engagement is through the bi-annual Team Member Engagement Survey administered by CultureAmp.  Engagement surveys are an important tool that gives team members an opportunity to provide feedback.  It also allows GitLab leadership to gain insight into what is important to team members.  

In October 2018 GitLab launched it's annual enagement survey via CultureAmp.  The survey was benchmarked against 2018 New Tech - Mid Size Companies (200-500 team members).   

## 2018 GitLab overall results

* 94% participation rate
* Overall engagement scores - 83% favorable, 12% neutral and 5% unfavorable

# Highest 3 Scores

* I am proud to work for GitLab - 95% favorable response
* I know how my work contribute to the goals of GitLab - 94% favorable response
* GitLab is in a position to really succeed over the next three years - 93% favorable response

# Highest 3 Scores vs Benchmark

* The leaders at GitLab have communicated a vision that motivates me - 88% favorable response and 22% above industry benchmark
* GitLab effectively directs resources (funding, people and efforts) towards company goals - 74% favorable response and 20% above industry benchmark
* At GitLab there is open and honest two-way communication - 84% favorable response and 20% above benchmark

#Lowest 3 Scores

* I have seen positive changes taking place based on recent employee survey results - 32% favorable response
* My manager or someone else has communicated clear actions based on recent team member survey results - 33% favorable response
* I believe my total compensation (base salary+any bonus+benefits+equity) is fair relative to similar roles at other companies - 43% favorable response

# Lowest 3 Score vs Benchmark

* I have seen positive changes taking place based on recent employee survey results - 32% favorable response and 12% below industry benchmark
* My manager or someone else has communicated clear actions based on recent team member survey results - 33% favorable response and 11% below industry benchmark
* I believe my total compensation (base salary+any bonus+benefits+equity) is fair relative to similar roles at other companies - 43% favorable response and 10% below industry benchmark

## October 2019 Engagement Survey Timeline

* October 14, 2019 - Survey launched via CultureAmp
* October 28th, 2019 - Survey closes
* Week of November 7th, 2019 - GitLab overall result shared with team members and with functional group results shared with functional leader
* November 11th - December 6th, 2019 - Functional group action plan for focus areas
* December 16th, 2019 - Functional action plans from survey added to OKRs

The survey consists of 46 questions divided into the following sections:

* GitLab Overall
* Company Confidence
* Our Leaders
* Your Manager
* Teamwork
* Your Role
* Culture
* Growth & Development
* Action
* Comments

## Benchmark and reporting

The 2019 survey will be benchmarked against New Tech Mid Size Companies (500+ team members).  The survey is completely anonymous! There are no names collecteed with any responses or comment.

# Results

Results will be shared with leaders who have 5 or more direct reports who complete the survey.  The E-team will identify one key action item to work during the 6 months between surveys.  
