---
layout: markdown_page
title: "Merchandise operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Community Relations manages the merchandise store. This includes:
- adding and removing items to and from the store,
- fulfilling orders,
- maintaining inventory levels and
- responding to store support requests

We are currently using the following vendors:
- Storefront:
  - Shopify
  - Amazon (trial)
- Inventory:
  - Printfection
  - Sendoso (merch leftovers)
  - Stickermule

## Access data

- URL: https://shop.gitlab.com
- Login: see 1Password secure note for details

## Operations

### Storefront vendors

#### Shopify

##### Access data

- URL: https://www.shopify.com
- Login: see 1Password secure note for details

### Amazon

#### Access data

- URL: https://sellercentral.amazon.com/
- Login: Use Community Relations 1Password credential from the marketing vault

## Inventory vendors

### Printfection

#### Access data

- URL: https://www.printfection.com/
- Login: Use Community Relations 1Password credential from the marketing vault.

### Stickermule

#### Access data

- URL: https://www.stickermule.com
- Login: see 1Password secure note for details