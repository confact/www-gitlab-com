---
layout: markdown_page
title: "Sales Systems"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Welcome to the Sales Systems Handbook

### Charter
* To support the Gitlab sales organization by providing a functional, stable and reliable platform for the teams to operate within. 
* To provide a user experience that enhances the various team members workflows
* To build the foundation of a scalable system that enables to continued growth of the sales team

### Sales Systems Issues
*  Create an issues in the [Sales Systems](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/issues) project
