---
layout: markdown_page
title: "Journeys"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

As a company we have to be great at providing multiple journeys.
The contributor, user, and buyer journey involve different people going through them, different metrics, different hand-offs, and different teams providing them.
Most companies never master a journey, a few companies master one: Ubuntu mastered the contributor journey, Atlassian the user journey, and Oracle the buyer journey.
Our ambition is to master all three.

The hard thing is that during a journey you have multiple hand-offs between departments and teams.
These hand-off points need to be well defined and work for both teams.
It is essential to measure all steps of the journey to see where an improvement would have the greatest effect.

## Contributor journey

A contributor is someone who contributed to GitLab with code, documentation, a blog article, a tweet, a talk, a meetup, and/or helping on the issue tracker/forum/irc.

1. Find GitLab (technical blog post)
1. Reach out to GitLab on social media (social media response)
1. Find information about contributing (contributor landing pages)
1. Start installation of GDK (multiple good options to run)
1. Finish installation of GDK (error free, good troubleshooting)
1. Submit a merge request (good documentation)
1. Get acknowledged (find first contributor)
1. Finish merge request (acknowledge first contribution, merge request coach)
1. Help someone else (forum, issue tracker)
1. Get recognized for a large contribution (named swag)
1. Get recognized as an core contributor (core contributor program, we need to know their location, maybe do this as part our forum)
1. Get invited to an event (match contributors with incoming requests for speakers, meetup organization, speaker content)
1. Write a blog post (help with editing)
1. Get activated after being inactive for a while (reactivation program)

People might skip parts of this journey, for example by never contributing code. That is perfectly fine.

## User journey

| Stage                    | Measured By     | GitLab Owner          |
|--------------------------|-----------------|-----------------------|
| Aware                    | ?               | Marketing             |
| Download                 | # Downloads     | Marketing?            |
| Install                  | # Usage Pings ? | Distribution          |
| Single User Experience   | Usage Ping      | UX                    |
| Invite Other Users       | Usage Ping      | ?                     |
| Adopt All Features       | Usage Ping      | ? Product & Marketing |
| Invite Other Departments | Usage Ping      | ?                     |
| Internal Reference       | ?               | ?                     |

## Buyer journey

| Stage              | Measured By                                                                                            | GitLab Owner          | Activities                                        |
|--------------------|--------------------------------------------------------------------------------------------------------|-----------------------|---------------------------------------------------|
| Awareness          | Impressions/Share of Voice                                                                             | Marketing             | Blog, SEO, Ads, Campaign, AR, PR                  |
| Initial Contact    | Get in touch with us                                                                                   | Marketing             | Trial, chat, or contact sales                     |
| Willing to Talk    |                                                                                                        | Marketing and Sales   | Initial Qualifying Meeting                        |
| Enter Buying Cycle | [Pipeline stage movement](/handbook/business-ops/#opportunity-stages)          | Sales                 | Progress through pipeline stages                  |
| Purchase           | Dollar amount in contract                                                                              | Sales, Finance, Legal | Sign our paper/submit PO                          |
| Adoption           | Usage ping of different DevOps stages                                                                  | Customer Success      |                                                   |
| Reference          | Scale of reference-ability: e.g. more points for video; then blog; public reference; private reference | Customer Advocates    | Published material or entry to reference database |
| Contribute         | More than a reference, the customer contributes to the GitLab project or community                     | Community Advocates   | Code; feature requests; Customer Advisory Boards  |
