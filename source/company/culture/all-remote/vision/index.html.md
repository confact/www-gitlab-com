---
layout: markdown_page
title: "Our long-term vision for remote work"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Our long-term vision for remote work

There are a few important outcomes we expect to see as remote work becomes even more prevalent around the world:

1. The majority of new startups intentionally form as all-remote companies.
1. Cities in developing countries, particularly in Africa, enabled by all-remote jobs at companies founded by local leaders.
1. Most startups in the Bay Area with a significant portion of their workforce working remotely.
1. Increased wages for remote work outside of metro areas.

## Why is this possible now?

All-remote work wouldn't be possible without the constant evolution of technology, and the tools that enable this type of work are continuously being developed and improved.

We aren't just seeing these impacts for all-remote companies. In fact, in some organizations with large campuses, employees will routinely do video calls instead of spending 10 minutes to go to a different building.

Here are some of the key factors that make all-remote work possible:

1. Faster internet everywhere - 100Mb/s+ cable, 5GHz Wi-Fi, 4G/LTE/5G cellular
1. Video call software - Google Hangouts, Zoom
1. Mobile technology - Everyone has a computer in their pocket
1. Evolution of speech-to-text conversion software - More accurate and faster than typing
1. Messaging apps - Slack, Mattermost, Zulip
1. Issue trackers - Trello, GitHub issues, GitLab issues
1. Suggestions - GitHub Pull Requests, GitLab Merge Requests
1. Static websites - GitHub Pages, GitLab Pages
1. English proficiency - More people are learning English
1. Increasing traffic congestion in cities
1. More demand for flexibility from new professionals entering the workforce

----

Return to the main [all-remote page](/company/culture/all-remote/).
