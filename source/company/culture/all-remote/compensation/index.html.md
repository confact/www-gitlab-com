---
layout: markdown_page
title: "All-Remote Compensation"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how compensation works in a global all-remote company.

## How do you pay people?

When you open your hiring pipeline to practically every nation on the planet, you get questions like "How do you pay people?" 

While there are certain complexities to paying team members who are spread across the globe, we believe that it's worthwhile. Being an all-remote company enables us to [hire the world's best talent](/company/culture/all-remote/hiring/), not just the best talent from a few cities.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Yr2do8A38r0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the above [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video, GitLab co-founder and CEO Sid Sijbrandij discusses the logistics of hiring remote team members with [Proof](https://prooftrading.com/) CEO [Daniel Aisen](https://twitter.com/dcaisen). 

Compensating people in a variety of countries isn't easy, and there's no blanket solution. However, as GitLab grows, it's able to incorporate in more countries. This allows us to be a traditional employer, paying local team members directly. 

For countries where that's not yet possible, we oftentimes hire professional employment organizations to serve as the employer of record (EOR) in order to facilitate payments. In other cases, we hire new team members as [contractors](/handbook/contracts/). 

We continually look at where our team is growing, which guides us in prioritizing where to incorporate next.

## What currency do you use?

The preference is for team members and future team members to be paid in their local currency. However, there may be instances where this is not possible and another currency may be chosen by the team member. For the full list of countries where GitLab can pay using local currency, visit the [Contracts section of our Handbook](/handbook/contracts/#available-currencies).

## Why do you pay local rates?

GitLab endeavours to [pay local rates](/handbook/people-group/global-compensation/#paying-local-rates) as opposed to paying a San Francisco wage to everyone in the company, regardless of their location. 

Our co-founder and CEO Sid Sijbrandij discusses this in a blog post entitled "[Why GitLab pays local rates](/2019/02/28/why-we-pay-local-rates/)."

> If we pay everyone the San Francisco wage for their respective roles, our compensation costs would increase greatly, and we would be forced to hire a lot fewer people. Then we wouldn’t be able to produce as much as we would like. If we started paying everyone the lowest rate possible, we would not be able to retain the people we want to keep.

Particularly in comparison to companies in cities with high costs of living, such as London, San Francisco, Singapore, Sydney, etc., an all-remote company realizes [a notable set of advantages](/handbook/people-group/global-compensation/#why-we-pay-local-rates) by hiring brilliant minds in locales with lower costs of living.

Funds that would be reserved for salaries — necessary to compete in hot labor markets — can be redirected to areas such as [research and development](/2018/06/12/how-ux-research-impacts-product-decisions/) (R&D), [acquisitions](/handbook/acquisitions/), and [hiring more people](/jobs/). 

These additional investments can be made simply by lowering a company's overall spend on compensation, but only if a company is willing to hire in locales where a dollar goes farther than the [SF Benchmark](/handbook/people-group/global-compensation/#sf-benchmark). 

## How do you decide how much to pay people?

Plainly, we want our compensation to be at a level were we can recruit and retain people who meet our requirements. A far more elaborate explaination can be found in the [Compensation Principles section of GitLab's Handbook](/handbook/people-group/global-compensation/#compensation-principles). 

Deciding how much we pay people who live and work in a multitude of regions across the globe is a continual process. [Iteration](/handbook/values/#iteration) and [Transparency](/handbook/values/#transparency) are values at GitLab, and both apply to compensation calculations. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/CgQNFffuHrc?start=34" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the above video, which was [streamed live](https://www.youtube.com/watch?v=CgQNFffuHrc) on [GitLab's YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg), GitLab Compensation & Benefits Manager Brittany Rohde discusses a variety of all-remote compensation topics with [Microverse](https://www.microverse.org/) COO [Juan José Mata](https://www.linkedin.com/in/jjmata).

### Understanding GitLab's Compensation Calculator

All-remote companies should consider a public compensation calcuator to remove guesswork. When you open roles to a global talent pool, this allows candidates to get familiar with compensation expectations prior to applying, which creates efficiencies in the hiring process. 

Colocated companies are generally relieved of this burden, as anyone applying to a role requiring them to reside in a given city can do their own research on market rates for that locale. 

### Iterating on compensation

During the fourth quarter of each year, GitLab conducts a compensation review to ensure all team members are paid based on market data in the [compensation calculator](https://about.gitlab.com/handbook/people-group/global-compensation/#compensation-calculator). This is not a [Cost of Living Adjustment](/handbook/people-group/global-compensation/#cost-of-living-adjustment-cola), but instead a review of the market changes.

For all-remote companies looking to enact a similar process, please study our [Annual Compensation Review](/handbook/people-group/global-compensation/#annual-compensation-review).

### Bolstering contractor compensation

Bolstering contractor compensation is an important tactic for emerging all-remote companies. For organizations that do not have the cashflow or scale to justify incorporating in a variety of countries (or pay a professional employment organizations to serve as the employer of record), you can hire team member globally as contractors and pay them a higher wage than you would a salaried employee.

A contractor may bear the costs of their own health insurance, social security taxes, and so forth. At GitLab, we provide a 17% higher compensation for the contractor. Visit our [Contracts page](/handbook/contracts/) to learn more about the different types of contracts we offer.

### Factoring the factors

For GitLab team members — with the exception of [directors or above](/handbook/people-group/global-compensation/#director-compensation) and anyone on a quota — the following factors are factored into their salary. We invite other all-remote companies to study this formula if considering how to pay a truly global team.

Your salary = [SF benchmark](/handbook/people-group/global-compensation/#sf-benchmark) x [Location Factor](/handbook/people-group/global-compensation/#location-factor) x [Level Factor](/handbook/people-group/global-compensation/#level-factor) x [Experience Factor](/handbook/people-group/global-compensation/#experience-factor) x [Contract Factor](/handbook/people-group/global-compensation/#contract-factor) x [Exchange Rate](/handbook/people-group/global-compensation/#exchange-rate)

## What role does experience play?

A person's background and experience influences their value to a company, regardless of how the company is organized. In an all-remote setting where a company pays local rates, experience can play an outsized role in a team member's cash compensation. 

It's important to evaluate experience *not* based on years in the workforce, but rather, demonstrated ability to drive projects and deliverables from the position description to completion. In an all-remote setting, this can include experience operating and managing in a work environment where you do not regularly meet direct reports, clients, and peers on an in-person basis. 

While experience factors at colocated companies typically focus on experience in the role, all-remote experience factors must consider more nuance — elements such as an ability to [communicate well virtually](/2019/08/05/tips-for-mastering-video-calls/) and operate effectively in an [asynchronous](/handbook/communication/) setting.

Learn more about [GitLab's Experience Factor](/handbook/people-group/global-compensation/#experience-factor).

## How do stock options factor in?

As many other organizations do, GitLab provides equity grants in the form of Incentive Stock Options (ISOs) and Non-Qualified Stock Options (NSOs). 

The difference in these two types of grants are, generally, as follows: ISOs are issued to US employees and carry a special form of tax treatment recognized by the US Internal Revenue Service (IRS). NSOs are granted to contractors and non-US employees.

You can learn more in the [Stock Options section of the Handbook](/handbook/stock-options/).

----

Return to the main [all-remote page](/company/culture/all-remote/).
