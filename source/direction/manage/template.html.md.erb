---
layout: markdown_page
title: Product Vision - Manage
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for the Manage stage of the DevOps lifecycle. If you'd
like to discuss this vision directly with the product manager for [Manage](/handbook/product/categories/#manage-stage),
feel free to reach out to Jeremy Watson via [e-mail](mailto:jwatson@gitlab.com),
[Twitter](https://twitter.com/gitJeremy), or by [scheduling a video call](https://calendly.com/jeremy_/gitlab-product-chat).

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=✓&state=opened&label_name[]=devops%3A%3Amanage).

Note that this stage is also involved in delivering improvements for our
[mobile development and delivery use case](/direction/mobile). If you have
an interest in mobile development using GitLab, please also take a look there.

## Overview

For administrators and executives, the process of management is always on. It extends to managing people, money, and risk; when the stakes are high, these stakeholders demand an experience and feature set that makes them feel in control. Setting up your processes shouldn’t be a struggle, and administrators shouldn’t have to compromise on security or compliance to make software work for them.

Not only do we want to fulfill those fundamental needs, we want to give you the freedom to work in new and powerful ways. We aspire to answer questions managers didn't know they had, and to automate away the mundane.

For these users, Manage's role in GitLab is to **help organizations prosper with configuration and analytics that enables them to work more efficiently**. It’s not enough to give instances the ability to meet their most basic needs; as a single application for the DevOps lifecycle, GitLab can exceed the standard and enable you to work in ways you previously couldn’t.

<%= partial("direction/categories", :locals => { :stageKey => "manage" }) %>

## Themes

We’re realizing this vision by delivering more powerful insights, making it easier than ever to do meaningful work in GitLab, and iterating on features that enables large organizations to thrive. We're also taking steps forward in improving traceability and security to ensure that GitLab can meet the needs of the enterprise, even in highly regulated environments. Finally, we're also improving the core experience for our end users and making existing features lovable.

### 🚢 Operating at scale

As GitLab continues to grow, we want to continue to iterate and improve on existing features. This is especially true of features that help large organizations thrive in GitLab.

We’re continuing to improve on authentication within GitLab, which is of critical importance for managing users at scale. We’re continuing to build out Group SAML for GitLab.com by automating membership and permissions management. We’re also improving OAuth by allowing you to programmatically manage tokens.

We’re also excited to give instances more control and power over how they manage spending. You’ll be able to clearly understand how your instance’s license is being used, with granular control over seats. Alongside making billing easier to understand than ever, we’re also improving our billing portal to give you the power to self-serve changes to your subscription.

Lastly, GitLab’s [single application](https://about.gitlab.com/handbook/product/#single-application) approach to DevOps puts the entire software development lifecycle in a single place. As a result, users don’t have to stitch together an overly-fragmented toolchain - and we want to go deeper in on that advantage with effortless, custom workflows.

### 🔔 Analytics and insights

As instances thrive, the amount of information flowing through GitLab grows exponentially. An administrator’s job quickly becomes more reliant on automation and tools to help them stay reactive (I need to respond to an urgent request for information) and proactive (tell me about areas of risk).

We'll find new opportunities to tell you something insightful and new about how you’re shipping software. We're doubling down on the power of analytics to provide insight into how instances can reduce cycle time and ship faster.

### 🔐 Security & compliance

“Trust, but verify”: GitLab makes it easy to contribute, but administrators should have comprehensive and consistent views on who is has done what. In short, changes in GitLab should be fully traceable.

After code gets merged, it may involve a host of individuals, commits, and objects - while we make it easy to go from idea to production, tracing that history back should also be a cinch. Since the heart of code change in GitLab is the merge request, we’ll add the ability to see the a deep history - including the issues, people, and commits - that led to the change.

Monitoring and traceability should be built deep into the application and allow GitLab to thrive in any regulated environment.

## Contributing to Manage

Our vision for Manage - and GitLab - is only achievable with help from the wider community. Many notable features for Manage have been [contributed by the community](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Manage&label_name[]=Community%20contribution), including [SAML](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/722/diffs)(contributed by [CERN](https://home.cern/)).

You can [contribute to Manage in many ways](https://about.gitlab.com/community/contribute/).

### Notable issues for contributions

* [Sharing a private project via URL](https://gitlab.com/gitlab-org/gitlab-ce/issues/20549)
* [Pinned projects on profile page](https://gitlab.com/gitlab-org/gitlab-ce/issues/45133)
* [Pinned projects and subgroups in groups](https://gitlab.com/gitlab-org/gitlab-ce/issues/28816)
* [Custom group emoji](https://gitlab.com/gitlab-org/gitlab-ce/issues/48907)

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "manage" }) %>

## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Manage can be viewed [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3Amanage), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

You can see further details on the prioritization and development process on the [page for the Manage team](https://about.gitlab.com/handbook/product/categories/manage/).
