title: Equinix
cover_image: '/images/blogimages/equinix-case-study-image.png'
cover_title: |
  Equinix increases the agility of their DevOps teams with self-serviceability and automation.
twitter_image: '/images/blogimages/equinix-case-study-image.png' 

customer_logo: '/images/case_study_logos/equinix-logo.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Redwood City, California, USA
customer_employees: 5,001 - 10,000

key_benefits:
  - |
    Self-serviceability among product owners, developers, and DevOps architects at Equinix
  - |
    Greater visibility into code changes, including when code was changed and who
    reviewed it
  - |
    Easy integration of GitLab with their existing DevOps tool stack
  - |
    Support for container-based workflows with GitLab’s built-in Docker Container Registry

customer_quote:
  quote_position: middle
  quote_text: Moving into a system like GitLab will help any organization or enterprise to get into DevOps methodology and  continuously improve the deployment workflows to achieve quality, agility, and self-serviceability.
  customer_name: Bala Kannan
  customer_title: Senior Software Engineer

customer_study_content:
  - title: THE CUSTOMER
    content:
      - |
        Equinix is the leading global data center company. With over 180+ colocation
        facilities across five continents, Equinix interconnects today’s largest global
        enterprises, enabling these companies to easily bring together employees,
        partners, and customers across the world. This case study focuses on
        Equinix’s client-side development teams, who are responsible for building
        software products and business critical applications.

  - title: THE CHALLENGE
    content:
      - |
        Like most companies serving global enterprise customers, development speed,
        self-serviceability and the ability to ship fixes and features in a timely
        manner are the primary reasons for Equinix to pursue new tools. To better
        meet the needs of their customers, Equinix, who is one of the early adopters
        of DevOps culture and best practices, realized the need to refine their
        tools to improve speed and agility.

      - |
        The development teams at Equinix were using Subversion (SVN), which
        initially met most of their needs. However, the team wanted to adopt more
        sophisticated DevOps practices, and needed a version control system that
        could provide self-serviceability and support distributed workflows to
        support globally distributed development teams. Ultimately, they wanted
        a solution that would help developers code better and faster to bring
        customers new features quickly.

      - |
        Equinix first started with an open source Git solution which checked the
        box on enabling distributed workflows. However, as the team matured and
        started defining detailed workflows, branching, and tagging strategies,
        they needed a more robust system that would meet their enterprise control
        and scaling needs. More specifically, they were looking for a tool that
        could handle granular authentication and authorization, enabled
        user management, and self-serviceability at enterprise scale. It also needed to be API enabled,
        to speed up CI workflows, and to support the organization's high availability and
        global scaling needs.

  - title: THE RESULTS
    content:
      - |
        With GitLab, Equinix can now ensure that every team follows defined
        development workflows, such as code check-in, code review, and continuous
        integration. Using GitLab’s advanced code review features, like merge
        request approvers and GitLab APIs, Equinix can be confident that all
        code is reviewed before it’s merged.

      - |
        Continuous integration is also a big part of the team’s workflow and they
        needed a way to trigger CI jobs in Jenkins, publish the code analysis,
        and provide visibility. Using GitLab’s Jenkins integration and GitLab APIs,
        the DevOps-Tools Engineering Team at Equinix was able to integrate
        multiple DevOps tools and products to enable CI/CD workflows.

      - |
        Equinix was successful in rolling out GitLab to enable specialized
        workflows along with self-serviceability for their development teams.
        Along with encouraging self-serviceability, Equinix, increased the
        agility of their developers and can ensure quality through automation. 
        The development teams view GitLab as a mature code review and code 
        management system.

      - |
        Equinix plans to use GitLab’s built-in Docker Container Registry,
        since they entered into containers in early 2015 and most of their
        custom workflows are containerized today. They focus on building
        containerized workloads which will support their global deployments. As
        Equinix looks to reduce the use of multiple systems serving the same
        purpose in their DevOps landscape, GitLab’s integrated Docker Container 
        Registry removes the need to have an independent registry and will make 
        it much easier for their development teams to upload, tag, refer and 
        share their container images.
